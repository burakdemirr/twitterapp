//
//  SignUpViewController.swift
//  TwitterApp
//
//  Created by burak on 3.03.2020.
//  Copyright © 2020 burak. All rights reserved.
//

import UIKit
import Firebase
class SignUpViewController: UIViewController {
    
    @IBOutlet var emailField: UITextField!
    @IBOutlet var paswdField: UITextField!
    @IBOutlet var rePasswdField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func signUp(_ sender: Any) {
        
        if(paswdField.text != rePasswdField.text){
            print("password is not match")
        }
            
        else if(!(emailField.text!.isEmpty) && !(paswdField.text!.isEmpty) && !(rePasswdField.text!.isEmpty)){
            Auth.auth().createUser(withEmail: emailField.text!, password: paswdField.text!) { authResult, error in
                // [START_EXCLUDE]
                if(error != nil){
                    print("error ",error!.localizedDescription)
                }
                    
                else{
                    print("\(self.emailField!) created")
                    self.emailField.text = ""
                    self.paswdField.text = ""
                    self.rePasswdField.text = ""
                }
                
            }
        }
    }
    
}


