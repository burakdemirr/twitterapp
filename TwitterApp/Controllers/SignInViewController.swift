//
//  SignInViewController.swift
//  
//
//  Created by burak on 3.03.2020.
//

import UIKit
import Firebase
class SignInViewController: UIViewController {
    
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func signInButton(_ sender: Any) {
        if(!(emailField.text?.isEmpty)! && !(passwordField.text?.isEmpty)!){
            Auth.auth().signIn(withEmail: emailField.text!, password: emailField.text!) { (user, error) in
                if(error != nil){
                    print(error!)
                }
                else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
                    self.present(homeVC, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let alert = UIAlertController(title: "Empty Fields!", message: "Please enter required fields", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: false)
            print("Please enter required fileds")
        }
        
        Auth.auth().signIn(withEmail: emailField.text!, password: emailField.text!) { (user, error) in
            
        }
        
        
    }
}


